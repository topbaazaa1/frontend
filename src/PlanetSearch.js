import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Route, withRouter} from 'react-router-dom';
import Planet from './Planet';
import axios from 'axios';
class PlanetSearch extends Component {
    constructor() {
        super();
        this.state = {
            Builds: [],
        };
    }
    componentDidMount() {
        let initialBuilds = [];
        axios.get('http://localhost:3001/SelectBuilding')
            .then(response => {
                return response.json();
            }).then(data => {
            initialBuilds = data.map((Build) => {
                return Build
            });
            console.log(initialBuilds);
            this.setState({
                Builds: initialBuilds,
            });
        });
    }
    render() {
        return (
                    <Planet state={this.state}/>
        );
    }
}
export default PlanetSearch;