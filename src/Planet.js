import React from 'react';
import PlanetSearch from './PlanetSearch';
import { connect } from 'react-redux';
// import createStore from './createReduxStore'
class Planet extends React.Component {
    constructor() {
        super();
    }

    render () {
        let Builds = this.props.state.Builds;
        let optionItems = Builds.map((Build) =>
                <option key={Build.Build}>{Build.Build}</option>
            );
            // const store = createStore()


        return (
            // <Provider>
         <div>
             <select>
                {optionItems}
             </select>
         </div>
        //  </Provider>
        )
    }
}

export default connect(null, PlanetSearch )(Planet);